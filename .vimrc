" === Vim Plug Plugins ===
"{{{
call plug#begin('~/.vim/plugged')

" Experimental Neovim LSP Setup For Elixir
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'ray-x/lsp_signature.nvim'

" Syntax & language servers
Plug 'neovimhaskell/haskell-vim'
Plug 'digitaltoad/vim-pug'
Plug 'MikauSchekzen/vim-tweego'
Plug 'neoclide/coc.nvim', {'branch': 'release'}, { 'for': ['javascript', 'svelte'] }
Plug 'coc-extensions/coc-svelte', {'do': 'yarn install --frozen-lockfile && yarn build'}
Plug 'iosmanthus/vim-nasm'
Plug 'ekalinin/dockerfile.vim'
Plug 'tikhomirov/vim-glsl'
Plug 'cespare/vim-toml'
Plug 'leafOfTree/vim-svelte-plugin', { 'for': 'svelte' }
Plug 'lervag/vimtex'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'elixir-tools/elixir-tools.nvim'

" Extensions
Plug 'junegunn/vim-easy-align'
Plug 'lifepillar/vim-mucomplete'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-commentary'
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }, { 'for': 'markdown' }
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'alvan/vim-closetag'
Plug 'skywind3000/asyncrun.vim'
Plug 'tpope/vim-surround'
Plug 'karb94/neoscroll.nvim'
Plug 'f-person/auto-dark-mode.nvim'
Plug 'mattn/emmet-vim'
Plug 'rmagatti/auto-session'
Plug 'rcarriga/nvim-notify'
Plug 'folke/noice.nvim'

" File system tree
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'MunifTanjim/nui.nvim',
Plug 'nvim-neo-tree/neo-tree.nvim', { 'branch': 'v2.x' }
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-file-browser.nvim'

" Themes
Plug 'NLKNguyen/papercolor-theme'
Plug 'lifepillar/vim-solarized8'
Plug 'whybin/Transient-256'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }

" Integration
Plug 'christoomey/vim-tmux-navigator'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

call plug#end()"}}}

" === Vim Settings ===
syntax on
filetype indent plugin on
set nocompatible
set number
language en_GB.UTF-8
if !has('nvim')
    set ttymouse=xterm2
end
set mouse=a

" Keep more in sessions
set sessionoptions+=winpos,terminal,folds

" keeps cursor in middle
set scrolloff=3

" highlight code blocks in markdown
augroup filetypeset
    autocmd BufNewFile,BufRead *.svx setlocal filetype=svelte
    au BufNewFile,BufReadPost *.md set filetype=markdown
augroup END
let g:markdown_fenced_languages = ['bash=sh', 'javascript', 'js=javascript', 'sql']

" make pasting auto-indents
nnoremap <leader>p p`[v`]=

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" Shortcut for Telescope File Browser
nnoremap <leader>tf <cmd>tabnew<cr><cmd>Telescope find_files<cr>
nnoremap <leader>fb <cmd>tabnew<cr><cmd>Telescope file_browser path=%:p:h select_buffer=true<cr>

" Remap vimwiki toggle
nmap <C-g> <Plug>VimwikiToggleListItem
vmap <C-g> <Plug>VimwikiToggleListItem

" Remap mucomplete tab
imap <c-j> <plug>(MUcompleteFwd)

" Shortcut for NERDTree
nmap <leader>ne :Neotree<cr>

" ------ Bird advice --------
inoremap jk <Esc>
set expandtab
set tabstop=2
set shiftwidth=2
set backspace=indent,eol,start
set foldmethod=marker
set wrap
set lbr
set showcmd
hi! link GitGutterAdd DiffAdd
hi! link GitGutterChange DiffChange
hi! link GitGutterDelete DiffDelete
set hlsearch
" No visual bell
set vb t_vb=
" Wildmenu
set wildmenu           " Command line completion highlighting
set wildmode=list:full " <Tab> on command line lists & cycles through matches

set termguicolors

" Default search case insensitive
set ignorecase
set smartcase

" Change cursor shape between insert and normal mode
" let &t_SI = "\<Esc>[5 q"
" let &t_EI = "\<Esc>[1 q"
" let &t_SI = "\<Esc>]12;orange\x7"
set textwidth=80
set colorcolumn=+1 " Set to 1 column after textwidth


" --- File Type Settings ---

" Set Ruby tabstop
au Filetype ruby set tabstop=2
" Set Javascript
autocmd Filetype javascript setlocal tabstop=4 shiftwidth=4 softtabstop=4

" --- Plugin Settings ---

" closetag ---
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.svelte, *.html.erb, *.erb'

" svelte
let g:vim_svelte_plugin_use_sass = 1
let g:vim_svelte_plugin_use_pug = 1

" vue
let g:vim_vue_plugin_use_sass = 1
let g:vim_vue_plugin_highlight_vue_attr = 1
let g:vim_vue_plugin_use_pug = 1

" haxokinase
let g:Hexokinase_highlighters = [ 'sign_column' ]

" Vim Latex Preview --
let g:livepreview_previewer = 'open -a Preview'
let g:tex_flavor='latex'
let g:livepreview_engine = 'xelatex'

augroup latex_live_preview
    autocmd BufWritePost *.tex AsyncRun open -a Preview && open -a Kitty 
augroup END

" Vim Session
let g:session_autoload = 'no'
let g:session_autosave = 'no'

" Solarized8 Theme
let g:solarized_visibility = 'high'

" VimWiki
nmap <Leader>in <Plug>VimwikiIndex

" Coc
" Map coc-fix-current (fixes current line from Rubocop + coc-solargraph)
nmap <C-f> <Plug>(coc-fix-current)
nmap <leader>t <Plug>(coc-rename)
nmap <C-j> <Plug>(coc-refactor)
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
" inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Run the Code Lens action on the current line
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')

" --- Fixes ---
" Fix menu highlight colour
autocmd ColorScheme * hi CocMenuSel guifg=#181822 guibg=#4545a6 guisp=#6666cc gui=undercurl ctermfg=235 ctermbg=62 cterm=NONE



" Rainbow Parentheses {{{
augroup rainbow_parens
  autocmd!
  autocmd FileType ruby,haskell RainbowParentheses
augroup END
" }}}

" -- bird function for joining paragraphs ---
function! JoinParagraphs()
    norm! Go
    global/.\n\n/norm! vipJ
endfunction

nnoremap <Leader>J :call JoinParagraphs()<Cr>

" -- bird theme --
colorscheme to_the_stars

" ----------
" Bird StatusLine {{{
" ----------
function! SetStatusLine()
    " Silence 'stray characters' from scrolling
    silent let status=' %f %#MatchParen# BUF %02n » COL %02c %#Underlined# '
                     \. system('git symbolic-ref --short HEAD 2> /dev/null '
                              \. '| xargs printf "*%s"')
    return status
endfunction
set statusline=%!SetStatusLine()
set laststatus=2  " Always show statusline
" ----------
" }}}
" ----------



" ---- Plugin config --------
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
set completeopt+=menuone,noinsert


" --- Indenting script by bird ---
" --------------
" IndentWord {{{
" --------------
function! s:GetTextCols(text, rgx, offset)
    let cols = []
    let nextIndex = [v:null, v:null, 0]

    while v:true
        let nextIndex = matchstrpos(a:text, a:rgx, nextIndex[2])
        if nextIndex[1] == -1
            break
        endif
        call add(cols, nextIndex[1] + a:offset)
    endwhile

    return cols
endfunction

function! IndentWord(toBack)
    let startLnum = line('.')
    if startLnum == 1
        echo 'Cannot align top line'
        return
    endif

    if len(getline('.')) == 0
        echo 'Cannot indent empty line'
        return
    endif

    " Setup vars
    let lnum = startLnum - 1
    let cols = []
    let bounds = [0, 0]
    let wordRgx = '\v(\w+|[^a-zA-Z0-9 ]+)'

    " Populate columns
    while lnum > 0
        let text = getline(lnum)
        if len(text) == 0
            let lnum -= 1
            continue
        endif

        " Left partition
        let lText = strpart(text, 0, bounds[0])
        let lCols = s:GetTextCols(lText, wordRgx, 0)
        if len(lCols) > 0
            let cols = lCols + cols
        endif

        " Right partition
        let rText = strpart(text, bounds[1])
        let rCols = s:GetTextCols(rText, wordRgx, bounds[1])
        if len(rCols) > 0
            let cols += rCols[0] > 0 && rCols[0] == bounds[1]
                        \? rCols[1:] : rCols
        endif

        if cols[0] == 0
            break
        endif

        let bounds = [cols[0], max([bounds[1], len(text) - 1])]
        let lnum -= 1
    endwhile

    " Determine new indent column
    let i = -1
    let targetFound = v:false
    let startIndex = matchstrpos(getline('.'), '\S')[1]

    while i < len(cols) - 1
        let i += 1

        if a:toBack
            if startIndex > cols[i]
                continue
            endif
            let targetCol = cols[max([0, i - v:count1])]
        else
            if startIndex >= cols[i]
                continue
            endif
            let targetCol = cols[min([len(cols), i + v:count1]) - 1]
        endif

        let targetFound = v:true
        break
    endwhile

    if !targetFound
        return
    endif

    " Modify current line
    let line = substitute(getline('.'), '\v^\s*', repeat(' ', targetCol), '')
    call setline('.', line)
    normal! ^
endfunction

nnoremap <silent> \w :<C-U>call IndentWord(v:false)<CR>
nnoremap <silent> \b :<C-U>call IndentWord(v:true)<CR>

" --------------
" }}}
" --------------

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


" Unmap shift arrows
nmap <S-Up> <Nop>
nmap <S-Down> <Nop>
vmap <S-Up> <Nop>
vmap <S-Down> <Nop>

" Unmap silently uncapitalising all my code
vmap u <nop>

" Italic support for tmux
set t_ZH=^[[3m
set t_ZR=^[[23m

" Fix HEEX support
au BufRead,BufNewFile *.eex,*.sface,*.lexs set filetype=eelixir
au BufRead,BufNewFile mix.lock set filetype=elixir

" Restore cursor shape?
" autocmd VimLeave * call system("print '\033[6 q'")
au VimLeave * set guicursor=a:ver100
